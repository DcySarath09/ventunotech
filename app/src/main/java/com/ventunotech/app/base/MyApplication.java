package com.ventunotech.app.base;

import android.app.Application;
import android.content.Context;

import com.ventunotech.app.uitls.VLog;

public class MyApplication extends Application {

    /*Application objects*/
    public static final String TAG = MyApplication.class.getSimpleName();
    private static Context context;
    public MyApplication() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        MyApplication.context = getApplicationContext();
        VLog.setVisible(true);
    }
}
