package com.ventunotech.app.ui.videolist;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ventunotech.app.R;
import com.ventunotech.app.service.VideosList;

import java.util.ArrayList;

public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.CustomViewHolder> {

    VideoListScreen videoListScreen;
    private ArrayList<VideosList.Track> videosLists;
    public VideoListAdapter(VideoListScreen videoListScreen,ArrayList<VideosList.Track> videosLists) {
        this.videosLists = videosLists;
        this.videoListScreen = videoListScreen;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_list_item, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        final VideosList.Track videosList = getItem(position);

        /*Bind View*/
        holder.nameTxt.setText(videosList.showname.trim());
        if(videosList.thumbpath!=null){
            Glide.with(videoListScreen).load(videosList.thumbpath).into(holder.thumbnailImg);
        }

        /*Click Event*/
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(videoListScreen, VideoPlayScreen.class);
                intent.putExtra("videoUrl",videosList.content_url);
                intent.putExtra("videoName",videosList.showname);
                videoListScreen.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return videosLists.size();
    }

    public VideosList.Track getItem(int position) {
        return videosLists.get(position);
    }

    protected class CustomViewHolder extends RecyclerView.ViewHolder {
        private TextView nameTxt;
        private ImageView thumbnailImg;
        private CardView cardView;
        public CustomViewHolder(View itemView) {
            super(itemView);
            nameTxt = itemView.findViewById(R.id.nameTxt);
            thumbnailImg = itemView.findViewById(R.id.thumbnailImg);
            cardView = itemView.findViewById(R.id.cardView);
        }
    }
}
