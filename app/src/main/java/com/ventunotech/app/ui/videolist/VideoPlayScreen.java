package com.ventunotech.app.ui.videolist;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.ventunotech.app.R;
import com.ventunotech.app.base.BaseActivity;
import com.ventunotech.app.databinding.VideoplayerScreenBinding;

import java.util.HashMap;
import java.util.Map;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class VideoPlayScreen extends BaseActivity implements View.OnClickListener {

    /*Create View*/
    VideoplayerScreenBinding binding;

    /*Create View Objects*/
    private int currentWindow = 0;
    private long playbackPosition = 0;
    private boolean currPlayWhenReady = true;
    private TrackSelector trackSelector;
    private SimpleExoPlayer simpleExoPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        initializePlayer();
        hideActionBar();
    }

    /*Bind the view*/
    private void initView() {
        binding = DataBindingUtil.setContentView(this,R.layout.videoplayer_screen);
        binding.nameTxt.setText(getIntent().getStringExtra("videoName"));

        binding.playerView.findViewById(R.id.exo_enter_fullscreen).setOnClickListener(this);
        binding.playerView.findViewById(R.id.exo_exit_fullscreen).setOnClickListener(this);
    }

    private void initializePlayer() {

        //Set Size
        binding.playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
        //Set Ratio
        int value = Resources.getSystem().getDisplayMetrics().widthPixels;
        binding.playerView.setLayoutParams(new FrameLayout.LayoutParams(value, (3 * value) / 4));
        binding.playerView.findViewById(R.id.container_fullscreen).setVisibility(VISIBLE);
        //Set controllers
        binding.playerView.showController();
        binding.playerView.setUseController(true);

        if (simpleExoPlayer == null) {
            trackSelector = new DefaultTrackSelector();
            simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
            binding.playerView.setPlayer(simpleExoPlayer);
            simpleExoPlayer.setPlayWhenReady(currPlayWhenReady);
            simpleExoPlayer.seekTo(currentWindow, playbackPosition);
        }

        //Pass Video URL
        setSource(getIntent().getStringExtra("videoUrl"));
    }

    /*Set MP4 Source file to play*/
    public void setSource(String source) {
        MediaSource mediaSource = buildMediaSource(source, null);
        if (mediaSource != null) {
            if (simpleExoPlayer != null) {
                simpleExoPlayer.prepare(mediaSource, true, false);
            }
        }
    }

    /*Checking source URL*/
    private MediaSource buildMediaSource(String source, HashMap<String, String> extraHeaders) {

        if (source == null) {
            toast("Input Is Invalid.");
            return null;
        }

        Uri uri = Uri.parse(source);
        if (uri == null || uri.getLastPathSegment() == null) {
            toast("Uri Converter Failed, Input Is Invalid.");
            return null;
        }

        DefaultHttpDataSourceFactory sourceFactory = new DefaultHttpDataSourceFactory("exoplayer-codelab");
        if (extraHeaders != null) {
            for (Map.Entry<String, String> entry : extraHeaders.entrySet())
                sourceFactory.getDefaultRequestProperties().set(entry.getKey(), entry.getValue());
        }

        return new ProgressiveMediaSource.Factory(sourceFactory)
                .createMediaSource(uri);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            hideSystemUi();
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) binding.playerView.getLayoutParams();
            params.width = params.MATCH_PARENT;
            params.height = params.MATCH_PARENT;
            binding.playerView.setLayoutParams(params);
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            showSystemUi();
            int value = Resources.getSystem().getDisplayMetrics().widthPixels;
            binding.playerView.setLayoutParams(new FrameLayout.LayoutParams(value, (3 * value) / 4));
        }
    }

    @SuppressLint("InlinedApi")
    private void hideSystemUi() {
        if (binding.playerView == null)
            return;

        binding.playerView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    @SuppressLint("InlinedApi")
    private void showSystemUi() {
        if (binding.playerView == null)
            return;
        binding.playerView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
    }

    @Override
    public void onClick(View view) {
        int targetViewId = view.getId();
        if (targetViewId == R.id.exo_enter_fullscreen) {
            enterFullScreen();
        } else if (targetViewId == R.id.exo_exit_fullscreen) {
            exitFullScreen();
        }
    }

    private void enterFullScreen() {
        binding.playerView.findViewById(R.id.exo_exit_fullscreen).setVisibility(VISIBLE);
        binding.playerView.findViewById(R.id.exo_enter_fullscreen).setVisibility(GONE);

        if (this != null)
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    private void exitFullScreen() {
        binding.playerView.findViewById(R.id.exo_exit_fullscreen).setVisibility(GONE);
        binding.playerView.findViewById(R.id.exo_enter_fullscreen).setVisibility(VISIBLE);

        if (this != null)
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }
}

