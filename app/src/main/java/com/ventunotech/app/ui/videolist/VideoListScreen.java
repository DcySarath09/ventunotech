package com.ventunotech.app.ui.videolist;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ventunotech.app.R;
import com.ventunotech.app.base.BaseActivity;
import com.ventunotech.app.databinding.VideolistScreenBinding;
import com.ventunotech.app.service.RequestID;
import com.ventunotech.app.service.ServerListener;
import com.ventunotech.app.service.ServerRequest;
import com.ventunotech.app.service.VideosList;

public class VideoListScreen extends BaseActivity implements ServerListener {

    /*Create View Objects*/
    VideolistScreenBinding binding;
    private VideoListAdapter videoListAdapter;

    /*API service objects*/
    ServerRequest serverRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        showBackArrow();
        setTitle("Home");

        /*Initialize objects*/
        serverRequest = ServerRequest.getInstance(this);

        /*Call API Service*/
        if (isConnectingToInternet()) {
            showProgressDialog();
            serverRequest.createRequest(this, RequestID.REQ_VIDEO_LIST);
        } else {
            noInternetAlertDialog();
        }
    }

    /*Bind the view*/
    private void initView() {
        binding = DataBindingUtil.setContentView(this,R.layout.videolist_screen);
        binding.videoListView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL,false));
    }


    @Override
    public void onSuccess(Object result, RequestID requestID) {
        hideProgressDialog();
        if(result!=null){
            VideosList videosList = (VideosList)result;
            if(videosList.tracklist.track.size() > 0){
                /*Create Videolist View*/
                videoListAdapter = new VideoListAdapter(this,videosList.tracklist.track);
                binding.videoListView.setAdapter(videoListAdapter);
                videoListAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onFailure(String error, RequestID requestID) {
        hideProgressDialog();
        toast(error);
    }
}
