package com.ventunotech.app.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import com.ventunotech.app.R;
import com.ventunotech.app.base.BaseActivity;
import com.ventunotech.app.ui.videolist.VideoListScreen;

public class SplashScreen extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        //Hide actionbar & status bar
        hideActionBar();
        transparentToolbar();
        lunchHomeScreen();
    }

    /*Open Main Screen*/
    private void lunchHomeScreen() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashScreen.this, VideoListScreen.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                finish();
            }
        },2000);
    }
}
