package com.ventunotech.app.uitls;

import android.text.TextUtils;
import android.util.Log;

public class VLog {

    private static boolean isVisible = true;

    private static final String TAG = "VENTUNOTECH::";

    public static void setVisible(boolean isVisible) {
        VLog.isVisible = isVisible;
    }

    public static void log(String msg) {
        if (isVisible && !TextUtils.isEmpty(msg))
            Log.v(TAG, msg);
    }

}
