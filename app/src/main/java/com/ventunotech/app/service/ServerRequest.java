package com.ventunotech.app.service;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.ventunotech.app.uitls.VLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;


public class ServerRequest {

    /*Create objects*/
    private static RequestQueue queue = null;
    public static ServerRequest serverRequest = null;
    public static ServerListener serverListener = null;
    Map<String, String> param;
    RequestID requestID;
    Context context;

    /*Create instance*/
    public ServerRequest(Context context) {
        if (queue == null) {
            queue = Volley.newRequestQueue(context);
        }
        this.context = context;
    }

    public static ServerRequest getInstance(Context context) {
        if (serverRequest == null) {
            serverRequest = new ServerRequest(context);
        }
        return serverRequest;
    }

    /*Calling method*/
    public void createRequest(ServerListener serverListener1,RequestID requestid) {

        serverListener = serverListener1;
        requestID = requestid;
        VLog.log("PASSING DATA" + param);

        StringRequest postRequest = new StringRequest(Request.Method.GET, getURL(),

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        VLog.log("RESPONSE" + response);
                        VLog.log("GET_URL" + getURL());

                        if (response != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                switch (requestID) {

                                    case REQ_VIDEO_LIST:
                                        serverListener.onSuccess(getJsonModelType(response), requestID);
                                        break;

                                    default:
                                        serverListener.onSuccess(getJsonModelType(response), requestID);
                                        break;
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                serverListener.onFailure("Please try again!", requestID);
                            }
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        serverListener.onFailure("Something went wrong...", requestID);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                return param;
            }

        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(postRequest);


    }

    //Get url based on requestID
    public String getURL() {
        String URL = null;

        switch (requestID) {

            case REQ_VIDEO_LIST:
                URL = Constents.VIDEO_URL;
                break;

            default:
                URL = Constents.VIDEO_URL;
                break;

        }

        return URL;
    }

    /*Get required model class*/
    private Class getModel() {
        Class model = null;
        switch (requestID) {

            case REQ_VIDEO_LIST:
                model = VideosList.class;
                break;

            default:
                model = String.class;
                break;
        }

        return model;
    }

    /*Response convertor*/
    private Object getJsonModelType(String data) {
        Object result = null;
        try {
            Gson gson = new Gson();
            result = gson.fromJson(data, getModel());
            VLog.log("success" + result);

        } catch (Exception e) {
            e.printStackTrace();
            VLog.log("ServerRequestHandler " + e);
        }
        return result;
    }
}
