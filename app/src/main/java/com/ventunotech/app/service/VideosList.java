package com.ventunotech.app.service;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class VideosList implements Serializable {

    @Expose
    @SerializedName("tracklist")
    public Tracklist tracklist;

    public static class Tracklist {
        @Expose
        @SerializedName("track")
        public ArrayList<Track> track;
        @Expose
        @SerializedName("totalvideos")
        public String totalvideos;
        @Expose
        @SerializedName("author")
        public String author;
        @Expose
        @SerializedName("title")
        public String title;
    }

    public static class Track {
        @Expose
        @SerializedName("jscode")
        public String jscode;
        @Expose
        @SerializedName("embed")
        public String embed;
        @Expose
        @SerializedName("videoduration")
        public int videoduration;
        @Expose
        @SerializedName("videoviews")
        public int videoviews;
        @Expose
        @SerializedName("posterpath")
        public String posterpath;
        @Expose
        @SerializedName("thumbpath")
        public String thumbpath;
        @Expose
        @SerializedName("publishdate")
        public String publishdate;
        @Expose
        @SerializedName("content_url")
        public String content_url;
        @Expose
        @SerializedName("content_provider_url")
        public String content_provider_url;
        @Expose
        @SerializedName("content_owner_type")
        public String content_owner_type;
        @Expose
        @SerializedName("privatetagname")
        public String privatetagname;
        @Expose
        @SerializedName("keywords")
        public String keywords;
        @Expose
        @SerializedName("showid")
        public int showid;
        @Expose
        @SerializedName("showname")
        public String showname;
        @Expose
        @SerializedName("category")
        public String category;
        @Expose
        @SerializedName("contentclassification")
        public String contentclassification;
        @Expose
        @SerializedName("description")
        public String description;
        @Expose
        @SerializedName("videoname")
        public String videoname;
        @Expose
        @SerializedName("key")
        public String key;
        @Expose
        @SerializedName("videoid")
        public String videoid;
    }
}
